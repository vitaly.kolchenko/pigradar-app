# Pig Radar

Simple app to persist and search map markers powered by **[Ktor](https://ktor.io/docs/welcome.html)** and **[Postgree](https://www.postgresql.org/)** with **[Postgis](https://postgis.net/)** extension. 

# Build and run

`git clone --recurse-submodules https://gitlab.com/vitaly.kolchenko/pigradar-app.git` \
`cd pigradar-app`

update HOST variable in [.env](.env) file with your host ip

(optional) update JWT_PRIVATE_KEY and JWK_OBJECT, to generate new variables use [genKeys.sh](auth-service/genKeys.sh)\
(optional) update DB_PASSWORD

`docker-compose up -d`

## [Android client](https://gitlab.com/vitaly.kolchenko/pig-radar-android.git)

`git clone --recurse-submodules https://gitlab.com/vitaly.kolchenko/pig-radar-android.git` \
`cd pig-radar-android` 

To install on connected device
`./gradlew installDebug -Pbase_url=<your_host_ip>`

To build apk: \
`./gradlew assebleDebug -Pbase_url=<your_host_ip>` \
Output : app/build/outputs/apk/debug

